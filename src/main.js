// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import App from './App';

import 'normalize.css/normalize.css';
import './styles/base.scss';
import './styles/helpers.scss';
import './styles/fonts.scss';
import './styles/header.scss';
import './styles/footer.scss';

Vue.config.productionTip = false;

import Vue from 'vue';
import VueCarousel from 'vue-carousel';
export const EventBus = new Vue();

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
});

Vue.use(VueCarousel);
