export const sharing = {
  facebook() {
    window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(window.location.href), 'Share on faceboook', 'width=400,height=300');
  },
  twitter() {
    window.open('https://twitter.com/home?status=' + encodeURIComponent(window.location.href), 'Share on twitter', 'width=400,height=300');
  },
  comments() {
    window.open('https://www.sme.sk/diskusie/3245056/1/slovensko-25-rokov.html?ref=25rokov', '_blank');
  }
}
