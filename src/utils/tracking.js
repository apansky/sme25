export const tracking = {
  navigation(trigger, year) {
    const evt = new CustomEvent('smeAnniversaryScreenNavigate', {
      detail: {
        trigger
      }
    });

    switch (year) {
      case 1991:
        evt.detail.screenName = 'homepage';
        break;
      case 2018:
        evt.detail.screenName = 'endpage';
        break;
      default:
        evt.detail.screenName = `year_${year}`;
    }

    window.dispatchEvent(evt);
  },
  share(trigger, year) {
    const evt = new CustomEvent('smeAnniversaryShare', {
      detail: {
        trigger
      }
    });

    switch (year) {
      case 1991:
        evt.detail.screenName = 'homepage';
        break;
      case 2018:
        evt.detail.screenName = 'final screen';
        break;
      default:
        evt.detail.screenName = `year_${year}`;
    }

    window.dispatchEvent(evt);
  }
};
