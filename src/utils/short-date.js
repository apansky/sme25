var characterMap = {
  "á": "a",
  "í": "i",
  "ú": "u",
  "ó": "o"
};

var chars = Object.keys(characterMap).join('|');
var allAccents = new RegExp(chars, 'g');

export function shortDate(string) {
  // const removed = string.replace(allAccents, function (match) {
  //   return characterMap[match];
  // });

  // return removed.substring(0, 7).toUpperCase();
  let title;
  if (parseInt(string) < 10) {
    title = 0 + string;
  } else {
    title = string;
  }

  return title.substring(0, 7).toUpperCase();
};
